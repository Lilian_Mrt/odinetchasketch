const container = document.querySelector(".container");
const width = window.innerWidth;
const height = window.innerHeight;
const buttonNumber = document.querySelector(".number");
const buttonClear = document.querySelector(".clear");
let dim;
(width<height)?dim=width-buttonNumber.offsetHeight-10:dim=height-buttonNumber.offsetHeight-10;
container.setAttribute("style", `width : ${dim}px; height : ${dim}px`);
let edge = 16;
buttonNumber.setAttribute("style",`margin-left : ${(dim - buttonNumber.offsetWidth)/2}px`);

function createGrid(edge){
    const dimElement = dim/edge;
    for (let i =0; i<edge; i++){
        for (let j=0; j<edge; j++){
            const newDiv = document.createElement("div");
            newDiv.classList.add("gridDiv");
            newDiv.setAttribute("style", `width : ${dimElement}px; height: ${dimElement}px; background-color: rgb(255,255,255)`);
            container.appendChild(newDiv);
        }
    }
    const allBoxes = container.querySelectorAll(".gridDiv");
    allBoxes.forEach((box) => {
      box.addEventListener("mouseover", () => hoverFunc(box));
    });
}
createGrid(edge);

function resizeGrid(){
    const width = window.innerWidth;
    const height = window.innerHeight;
    let dim;
    (width<height)?dim=width-buttonNumber.offsetHeight-10:dim=height-buttonNumber.offsetHeight-10;
    container.style.width = `${dim}px`;
    container.style.height = `${dim}px`;
    const dimElement = dim / edge;
    const allBoxes = container.querySelectorAll(".gridDiv");
    allBoxes.forEach((box) =>{
        box.style.width = `${dimElement}px`;
        box.style.height = `${dimElement}px`;
    })
    buttonNumber.style.marginLeft = `${(dim - buttonNumber.offsetWidth)/2}px`;
}

window.addEventListener("resize",() => resizeGrid());


function hoverFunc(box){
    var rgb = box.style.backgroundColor.match(/\d+/g)[0];
    if (rgb != 0){
        box.style.backgroundColor = `rgb(${rgb - 25.5},${rgb - 25.5},${rgb - 25.5})`; 
    }
}

buttonNumber.addEventListener("click",() => {
    let newEdge = prompt("Give a number less than 100");
    while (newEdge>100 || newEdge<1){
        newEdge = prompt("Care. Give an integer between 1 and 100")
    }
    const allBoxes = container.querySelectorAll(".gridDiv");
    allBoxes.forEach((box) => {
        container.removeChild(box);
    })
    createGrid(newEdge);
});

buttonClear.addEventListener("click",() => {
    const allBoxes = container.querySelectorAll(".gridDiv");
    allBoxes.forEach((box) => {
        box.style.backgroundColor = "rgb(255,255,255)"
    })
});